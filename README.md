Maven Test POC
===============

POC to demonstrate release management with maven

The release plugin works in two steps:

1. mvn release:prepare

2. mvn release:perform

The release:prepare goal will:

  * Verify that there are no uncommitted changes in the workspace.
  * Prompt the user for the desired tag, release and development version names.
  * Modify and commit release information into the pom.xml file.
  * Tag the entire project source tree with the new tag name.

It needs the location of the repository

The release:perform goal will:

  * Extract file revisions versioned under the new tag name.
  * Execute the maven build lifecycle on the extracted instance of the project.
  * Deploy the versioned artifacts to appropriate local and remote repositories.

It needs a remote repository (can be bitbucket, ftp or S3 location)
credentials can be stored in the settings.xml

Infra strucuture needed
----------------------
On top of as source control system (git on bitbucket)
build machine with maven installed and access to source control system and repository location (local server or Amazon EC2 instance)
repository location to put generated release artifacts (accesible for developers, system maintainance, testers etc. (Amazon S3)

